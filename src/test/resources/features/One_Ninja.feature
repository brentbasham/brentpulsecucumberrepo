Feature: One Ninja
  There is only room for one...

    # humbly borrowed from Cucumber docs (slightly modified)
    Scenario: Only One -- More than one alive
      Given there are three ninjas
      And there is more than one ninja alive
      When 2 ninjas meet, they will fight
      Then one ninja dies (but not me!)
      And there is one ninja less alive

    Scenario: Only One -- One alive
      Given there is only 1 ninja alive
      When anyone else comes to challenge (except Chuck Norris)
      And bring any weapon they choose
      Then they will perish and the ninja will live forever - or will he?